[constants]
arch = 'armv7a'
toolchain_prefix = 'arm-linux-gnueabihf-'

[binaries]
cpp = ['ccache', toolchain_prefix + 'g++']
strip = toolchain_prefix + 'strip'

[built-in options]
cpp_link_args = ['-static']

[host_machine]
system = 'linux'
cpu_family = 'arm'
cpu = arch
endian = 'little'

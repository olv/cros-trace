[constants]
arch = 'aarch64'
toolchain_prefix = arch + '-linux-gnu-'

[binaries]
cpp = ['ccache', toolchain_prefix + 'g++']
strip = toolchain_prefix + 'strip'

[built-in options]
cpp_link_args = ['-static']

[host_machine]
system = 'linux'
cpu_family = arch
cpu = arch
endian = 'little'

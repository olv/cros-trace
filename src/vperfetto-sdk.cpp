// Copyright 2020 The Android Open Source Project
// SPDX-License-Identifier: Apache-2.0

#include "vperfetto.h"
#include "vperfetto-util.h"
#include "perfetto_trace.pb.h"

#include <functional>
#include <random>
#include <string>
#include <fstream>
#include <unordered_map>

namespace vperfetto {

struct TraceCpuTimeSync {
    bool hasData() const { return cpuTime != 0 && clockTime != 0 && clockId != 0; }

    uint64_t cpuTime;
    double cpuCyclesPerNano;
    uint64_t clockTime;
    uint32_t clockId;
};

static void mutateTracePackets(::perfetto::protos::Trace& pbtrace,
    std::function<void(::perfetto::protos::TracePacket* packet)> mutator) {
    for (int i = 0; i < pbtrace.packet_size(); ++i) {
        auto* packet = pbtrace.mutable_packet(i);
        mutator(packet);
    }
}

static void iterateTraceTimestamps(
    ::perfetto::protos::Trace& pbtrace,
    std::function<uint64_t(uint64_t)> forEachTimestamp,
    std::function<uint64_t(uint64_t)> forEachRealtimeTimestamp) {

    for (int i = 0; i < pbtrace.packet_size(); ++i) {
        auto* packet = pbtrace.mutable_packet(i);
        if (packet->has_timestamp()) {
            packet->set_timestamp(forEachTimestamp(packet->timestamp()));
        }

        if (packet->has_ftrace_events()) {
            auto* ftevts = packet->mutable_ftrace_events();
            for (int j = 0; j < ftevts->event_size(); ++j) {
                auto* ftev = ftevts->mutable_event(j);
                if (ftev->has_timestamp()) {
                    ftev->set_timestamp(forEachTimestamp(ftev->timestamp()));
                }
            }
        }

        if (packet->has_android_log()) {
            auto* pt = packet->mutable_android_log();
            for (int j = 0; j < pt->events_size(); ++j) {
                auto* ev = pt->mutable_events(j);
                if (ev->has_timestamp()) {
                    ev->set_timestamp(forEachRealtimeTimestamp(ev->timestamp()));
                }
            }
        }
    }
}

static void iterateTraceTrackDescriptorUuids(
    ::perfetto::protos::Trace& pbtrace,
    std::function<uint64_t(uint64_t)> forEachUuid) {

    for (int i = 0; i < pbtrace.packet_size(); ++i) {
        auto* packet = pbtrace.mutable_packet(i);

        // trace packet defaults
        if (packet->has_trace_packet_defaults()) {
            auto* tpd = packet->mutable_trace_packet_defaults();
            if (tpd->has_track_event_defaults()) {
                auto* ted = tpd->mutable_track_event_defaults();
                if (ted->has_track_uuid()) {
                    ted->set_track_uuid(forEachUuid(ted->track_uuid()));
                }
            }
        }

        // individual track events
        if (packet->has_track_event()) {
            auto* te = packet->mutable_track_event();
            if (te->has_track_uuid()) {
                te->set_track_uuid(forEachUuid(te->track_uuid()));
            }
        }

        // track descriptors
        if (packet->has_track_descriptor()) {
            auto* trd = packet->mutable_track_descriptor();
            if (trd->has_uuid()) {
                trd->set_uuid(forEachUuid(trd->uuid()));
            }
            if (trd->has_parent_uuid()) {
                trd->set_parent_uuid(forEachUuid(trd->parent_uuid()));
            }
        }
    }
}

// Replace PID in "X|PID..."
std::string replace_pid(std::string buf,
                        std::function<int32_t(int32_t)> forEachPid) {
    // parse "X|PID..."
    size_t p1 = buf.find_first_of("|");
    if (p1 == std::string::npos)
        return buf;
    ++p1;
    size_t p2 = buf.find_first_not_of("0123456789", p1);
    std::string buf2 = buf;
    if (p2 != std::string::npos)
        buf2[p2] = '\0';
    int32_t old_pid = atoi(buf2.c_str() + p1);
    int32_t new_pid = forEachPid(old_pid);
    // gen new string
    buf2 = buf.substr(0, p1);
    buf2 += std::to_string(new_pid);
    if (p2 != std::string::npos)
        buf2 += buf.substr(p2);
    return buf2;
}

static uint64_t
genUuid()
{
    static std::minstd_rand rng(time(NULL));
    static std::uniform_int_distribution<uint64_t> dist;
    return dist(rng);
}

// A higher-order function to conveniently iterate over all sequence ids and pids.
// TODO: What other Ids are important?
// This also takes care of changing UUIDs if a process or thread descriptor gets its id modified.
static void iterateTraceIds(
    ::perfetto::protos::Trace& pbtrace,
    std::function<uint32_t(uint32_t)> forEachTrustedUid,
    std::function<uint32_t(uint32_t)> forEachSequenceId,
    std::function<int32_t(int32_t)> forEachPid,
    std::function<int32_t(int32_t)> forEachTid,
    std::function<int32_t(int32_t)> forEachCpu) {

    bool needRemapUuids = false;
    std::unordered_map<uint64_t, uint64_t> uuidMap;

    std::function<int32_t(int32_t, uint64_t)> transformPidWithUuidRemapTracking =
        [&needRemapUuids, &uuidMap, forEachPid](int32_t prev, uint64_t prevUuid) {
        int32_t next = forEachPid(prev);
        if (prev == next) return next;
        if (uuidMap.find(prevUuid) != uuidMap.end()) return next;
        uuidMap[prevUuid] = genUuid();
        needRemapUuids = true;
        return next;
    };

    for (int i = 0; i < pbtrace.packet_size(); ++i) {
        auto* packet = pbtrace.mutable_packet(i);

        if (packet->has_trusted_uid()) {
            packet->set_trusted_uid(
                forEachTrustedUid(packet->trusted_uid()));
        }

        if (packet->has_trusted_packet_sequence_id()) {
            packet->set_trusted_packet_sequence_id(
                forEachSequenceId(packet->trusted_packet_sequence_id()));
        }

        if (packet->has_ftrace_events()) {
            auto* ftevts = packet->mutable_ftrace_events();

            if (ftevts->has_cpu()) {
                ftevts->set_cpu(
                    forEachCpu(ftevts->cpu()));
            }

            for (int j = 0; j < ftevts->event_size(); ++j) {
                auto* ftev = ftevts->mutable_event(j);

                if (ftev->has_pid()) {
                    ftev->set_pid(
                        forEachPid(ftev->pid()));
                }

                if (ftev->has_print()) {
                    auto* print = ftev->mutable_print();
                    if (print->has_buf()) {
                        print->set_buf(replace_pid(print->buf(), forEachPid));
                    }
                }

                if (ftev->has_task_rename()) {
                    auto* tr = ftev->mutable_task_rename();
                    if (tr->has_pid()) {
                        tr->set_pid(forEachPid(tr->pid()));
                    }
                }

                if (ftev->has_sched_switch()) {
                    auto* sw = ftev->mutable_sched_switch();
                    if (sw->has_prev_pid() && (sw->prev_pid() != 0)) {
                        sw->set_prev_pid(forEachPid(sw->prev_pid()));
                    }
                    if (sw->has_next_pid() && (sw->next_pid() != 0)) {
                        sw->set_next_pid(forEachPid(sw->next_pid()));
                    }
                }

                if (ftev->has_sched_wakeup()) {
                    auto* wakeup = ftev->mutable_sched_wakeup();
                    if (wakeup->has_pid() && (wakeup->pid() != 0)) {
                        wakeup->set_pid(forEachPid(wakeup->pid()));
                    }
                    if (wakeup->has_target_cpu()) {
                        wakeup->set_target_cpu(
                            forEachPid(wakeup->target_cpu()));
                    }
                }

                if (ftev->has_sched_blocked_reason()) {
                    auto* blockedreason = ftev->mutable_sched_blocked_reason();
                    if (blockedreason->has_pid() && (blockedreason->pid() != 0)) {
                        blockedreason->set_pid(forEachPid(blockedreason->pid()));
                    }
                }

                if (ftev->has_sched_waking()) {
                    auto* waking = ftev->mutable_sched_waking();
                    if (waking->has_pid() && (waking->pid() != 0)) {
                        waking->set_pid(forEachPid(waking->pid()));
                    }
                    if (waking->has_target_cpu()) {
                        waking->set_target_cpu(
                            forEachPid(waking->target_cpu()));
                    }
                }

                if (ftev->has_sched_wakeup_new()) {
                    auto* evt = ftev->mutable_sched_wakeup_new();
                    if (evt->has_pid() && (evt->pid() != 0)) {
                        evt->set_pid(forEachPid(evt->pid()));
                    }
                    if (evt->has_target_cpu()) {
                        evt->set_target_cpu(
                            forEachPid(evt->target_cpu()));
                    }
                }

                if (ftev->has_sched_process_exec()) {
                    auto* evt = ftev->mutable_sched_process_exec();
                    if (evt->has_pid() && (evt->pid() != 0)) {
                        evt->set_pid(forEachPid(evt->pid()));
                    }
                    if (evt->has_old_pid() && (evt->old_pid() != 0)) {
                        evt->set_old_pid(forEachPid(evt->old_pid()));
                    }
                }

                if (ftev->has_sched_process_exit()) {
                    auto* evt = ftev->mutable_sched_process_exit();
                    if (evt->has_pid() && (evt->pid() != 0)) {
                        evt->set_pid(forEachPid(evt->pid()));
                    }
                    if (evt->has_tgid() && (evt->tgid() != 0)) {
                        evt->set_tgid(forEachPid(evt->tgid()));
                    }
                }

                if (ftev->has_sched_process_fork()) {
                    auto* evt = ftev->mutable_sched_process_fork();
                    if (evt->has_parent_pid() && (evt->parent_pid() != 0)) {
                        evt->set_parent_pid(forEachPid(evt->parent_pid()));
                    }
                    if (evt->has_child_pid() && (evt->child_pid() != 0)) {
                        evt->set_child_pid(forEachPid(evt->child_pid()));
                    }
                }

                if (ftev->has_sched_process_free()) {
                    auto* evt = ftev->mutable_sched_process_free();
                    if (evt->has_pid() && (evt->pid() != 0)) {
                        evt->set_pid(forEachPid(evt->pid()));
                    }
                }

                if (ftev->has_sched_process_hang()) {
                    auto* evt = ftev->mutable_sched_process_hang();
                    if (evt->has_pid() && (evt->pid() != 0)) {
                        evt->set_pid(forEachPid(evt->pid()));
                    }
                }

                if (ftev->has_sched_process_wait()) {
                    auto* evt = ftev->mutable_sched_process_wait();
                    if (evt->has_pid() && (evt->pid() != 0)) {
                        evt->set_pid(forEachPid(evt->pid()));
                    }
                }
            }
        }

        if (packet->has_track_descriptor()) {
            auto* trd = packet->mutable_track_descriptor();

            if (trd->has_process()) {
                auto* p = trd->mutable_process();
                if (p->has_pid()) {
                    p->set_pid(
                        transformPidWithUuidRemapTracking(p->pid(), trd->uuid()));
                }
            }

            if (trd->has_thread()) {
                auto* t = trd->mutable_thread();
                if (t->has_pid()) {
                    t->set_pid(
                        transformPidWithUuidRemapTracking(t->pid(), trd->uuid()));
                }
                if (t->has_tid()) {
                    t->set_tid(forEachTid(t->tid()));
                }
            }
        }

        if (packet->has_process_tree()) {
            auto* pt = packet->mutable_process_tree();
            for (int j = 0; j < pt->processes_size(); ++j) {
                auto* p = pt->mutable_processes(j);
                if (p->has_pid()) {
                    p->set_pid(forEachPid(p->pid()));
                }
                if (p->has_ppid()) {
                    p->set_ppid(forEachPid(p->ppid()));
                }
            }

            for (int j = 0; j < pt->threads_size(); ++j) {
                auto* t = pt->mutable_threads(j);
                if (t->has_tid()) {
                    t->set_tid(forEachTid(t->tid()));
                }
                if (t->has_tgid()) {
                    t->set_tgid(forEachPid(t->tgid()));
                }
            }
        }

        if (packet->has_android_log()) {
            auto* al = packet->mutable_android_log();
            for (int j = 0; j < al->events_size(); ++j) {
                auto* ev = al->mutable_events(j);
                if (ev->has_pid()) {
                    ev->set_pid(forEachPid(ev->pid()));
                }
                if (ev->has_tid()) {
                    ev->set_tid(forEachTid(ev->tid()));
                }
            }
        }
    }

    if (needRemapUuids) {
        fprintf(stderr, "%s: need to remap uuids...\n", __func__);
        iterateTraceTrackDescriptorUuids(
            pbtrace,
            [&uuidMap](uint64_t uuid) {
            if (uuidMap.find(uuid) == uuidMap.end()) {
                fprintf(stderr, "%s: Warning: did not catch uuid %llu from previous trace. Was this a parent_uuid? If so, note that the trace was generated like this in the first place (with missing parent_uuid for a track descriptor)\n", __func__, (unsigned long long)uuid);
            }
            return uuidMap[uuid];
        });
    }
}

static void sCalcMaxIds(
    ::perfetto::protos::Trace& pbtrace,
    uint32_t* maxTrustedUidOut,
    uint32_t* maxSequenceIdOut,
    uint32_t* maxPidOut,
    uint32_t* maxTidOut,
    uint32_t* maxCpuOut) {

    uint32_t maxSequenceId = 0;
    uint32_t maxPid = 0;
    uint32_t maxTrustedUid = 0;
    uint32_t maxTid = 0;
    uint32_t maxCpu = 0;

    *maxSequenceIdOut = maxSequenceId;
    *maxPidOut = maxPid;
    *maxTrustedUidOut = maxTrustedUid;
    *maxTidOut = maxTid;
    *maxCpuOut = maxCpu;;

    iterateTraceIds(pbtrace,
        [&maxTrustedUid](uint32_t uid) {
            if (uid > maxTrustedUid) {
                maxTrustedUid = uid;
            }
            return uid;
        },
        [&maxSequenceId](uint32_t seqid) {
            if (seqid > maxSequenceId) {
                maxSequenceId = seqid;
            }
            return seqid;
        },
        [&maxPid](uint32_t pid) {
            if (pid > maxPid) {
                maxPid = pid;
            }
            return pid;
        },
        [&maxTid](uint32_t tid) {
            if (tid > maxTid) {
                maxTid = tid;
            }
            return tid;
        },
        [&maxCpu](uint32_t cpu) {
            if (cpu > maxCpu) {
                maxCpu = cpu;
            }
            return cpu;
        });

    fprintf(stderr, "%s: trace's max trusted uid %u seq %u pid %u\n", __func__, maxTrustedUid, maxSequenceId, maxPid);

    *maxTrustedUidOut = maxTrustedUid;
    *maxSequenceIdOut = maxSequenceId;
    *maxPidOut = maxPid;
    *maxTidOut = maxTid;
    *maxCpuOut = maxCpu;;
}

static void getClockSnapshotTimes(const ::perfetto::protos::Trace& pbtrace, uint64_t* realtime, uint64_t* boottime) {
    using ::perfetto::protos::BuiltinClock;
    for (int i = 0; i < pbtrace.packet_size(); ++i) {
        const auto& packet = pbtrace.packet(i);
        if (packet.has_clock_snapshot()) {
            auto snapshot = packet.clock_snapshot();
            for (int c = 0; c < snapshot.clocks_size(); ++c) {
                if (snapshot.clocks(c).clock_id() == BuiltinClock::BUILTIN_CLOCK_BOOTTIME)
                    *boottime = snapshot.clocks(c).timestamp();
                if (snapshot.clocks(c).clock_id() == BuiltinClock::BUILTIN_CLOCK_REALTIME)
                    *realtime = snapshot.clocks(c).timestamp();
            }
            if (*realtime != 0 && *boottime != 0) {
                return;
            }
        }
    }
}

// Transforms addonTrace timestamps into mainTrace space and merges with mainTrace.
static std::vector<char> constructCombinedTrace(
    const std::vector<char>& mainTrace,
    const std::vector<char>& addonTrace,
    int64_t mainTimeDiff, bool addTraces) {

    ::perfetto::protos::Trace main_pbtrace;
    {
        std::string traceStr(mainTrace.begin(), mainTrace.end());
        if (!main_pbtrace.ParseFromString(traceStr)) {
            fprintf(stderr, "%s: Failed to parse protobuf as a string\n", __func__);
            return {};
        }
    }

    // Calculate the max seqid/pid/tid in the main
    uint32_t maxMainTrustedUid = 0;
    uint32_t maxMainSequenceId = 0;
    uint32_t maxMainPid = 0;
    uint32_t maxMainTid = 0;
    uint32_t maxMainCpu = 0;

    sCalcMaxIds(main_pbtrace, &maxMainTrustedUid, &maxMainSequenceId, &maxMainPid, &maxMainTid, &maxMainCpu);

    // Use the same offset in case pids and tids are mixed up.
    uint32_t maxPidTid = std::max(maxMainPid, maxMainTid);
    // Use an offset that is easy to read out the original addon pid for debugging.
    uint64_t pidTidOffset = 1000000;
    while(pidTidOffset < maxPidTid)
        pidTidOffset *= 10;

    // Easier to see host CPUs vs guest CPUs with fixed offset.
    // 1000 would be more ideal, but the Perfetto UI doesn't allow CPU IDs that high.
    int32_t addonCpuOffset = 100;

    ::perfetto::protos::Trace addon_pbtrace;
    {
        std::string traceStr(addonTrace.begin(), addonTrace.end());
        if (!addon_pbtrace.ParseFromString(traceStr)) {
            fprintf(stderr, "%s: Failed to parse protobuf as a string\n", __func__);
            return {};
        }
    }

    uint64_t addonRealtimeToBoottime = 0;
    uint64_t mainBoottimeToRealtime = 0;
    uint64_t addonRealtimeToMainRealtime = 0;
    uint64_t realtime = 0;
    uint64_t boottime = 0;

    getClockSnapshotTimes(main_pbtrace, &realtime, &boottime);
    mainBoottimeToRealtime = realtime - boottime;
    getClockSnapshotTimes(addon_pbtrace, &realtime, &boottime);
    addonRealtimeToBoottime = boottime - realtime;
    addonRealtimeToMainRealtime = addonRealtimeToBoottime + mainTimeDiff + mainBoottimeToRealtime;

    fprintf(stderr, "%s: postprocessing trace with main time diff of %lld, and offseting by main max seqid %u, pid offset %" PRIu64 "\n", __func__,
            (long long)mainTimeDiff,
            maxMainSequenceId,
            pidTidOffset);
    if (!addTraces) {
        mutateTracePackets(addon_pbtrace,
            [](auto* packet) {
                bool needReplace = false;
                if (packet->has_clock_snapshot()) {
                    packet->clear_clock_snapshot();
                }
                if (packet->has_service_event()) {
                    packet->clear_service_event();
                }
                if (needReplace) {
                    auto* te = packet->mutable_track_event();
                    te->set_type((::perfetto::protos::TrackEvent_Type)(0));
                }
            });

        iterateTraceTimestamps(addon_pbtrace,
            [mainTimeDiff](uint64_t ts) {
                return ts + mainTimeDiff;
            },
            [addonRealtimeToMainRealtime](uint64_t ts) {
                return ts + addonRealtimeToMainRealtime;
            });

        iterateTraceIds(addon_pbtrace,
            [maxMainTrustedUid](uint32_t uid) {
                return uid + maxMainTrustedUid;
            },
            [maxMainSequenceId](uint32_t seqid) {
                return seqid + maxMainSequenceId;
            },
            [pidTidOffset](int32_t pid) {
                if (pid == 0) return 0;
                return (int32_t)(pid + pidTidOffset);
            },
            [pidTidOffset](int32_t tid) {
                if (tid == 0) return 0;
                return (int32_t)(tid + pidTidOffset);
            },
            [addonCpuOffset](int32_t cpu) {
                return cpu + addonCpuOffset;
            });
    }
    std::string traceAfter;
    addon_pbtrace.SerializeToString(&traceAfter);

    std::vector<char> combined;
    combined.resize(mainTrace.size() + traceAfter.size());
    memcpy(combined.data(), mainTrace.data(), mainTrace.size());
    memcpy(combined.data() + mainTrace.size(), traceAfter.data(), traceAfter.size());
    // memcpy(combined.data(), traceAfter.data(), traceAfter.size());
    // memcpy(combined.data() + traceAfter.size(), mainTrace.data(), mainTrace.size());
    // combined.resize(traceAfter.size());
    // memcpy(combined.data(), traceAfter.data(), traceAfter.size());

    return combined;
}

static bool getTraceCpuTimeSync(const std::vector<char>& trace, TraceCpuTimeSync* retCpuTime) {
    ::perfetto::protos::Trace pbtrace;
    std::string traceStr(trace.begin(), trace.end());
    if (!pbtrace.ParseFromString(traceStr)) {
        return false;
    }

    TraceCpuTimeSync first = {0};
    TraceCpuTimeSync last = {0};
    for (int i = 0; i < pbtrace.packet_size(); ++i) {
        TraceCpuTimeSync found = {0};
        const auto* packet = &pbtrace.packet(i);
        uint32_t boottime_clockid = static_cast<uint32_t>(::perfetto::protos::BuiltinClock::BUILTIN_CLOCK_BOOTTIME);
        /* find timesync events generated by timesync tool */
        if (packet->has_track_event() && packet->track_event().debug_annotations_size() > 0) {
            for (int d = 0; d < packet->track_event().debug_annotations_size(); ++d) {
                const auto& data = packet->track_event().debug_annotations(d);
                if (data.name() == "clock_sync_boottime") {
                    found.clockId = boottime_clockid;
                    found.clockTime = data.uint_value();
                } else if (data.name() == "clock_sync_cputime") {
                    found.cpuTime = data.uint_value();
                }
            }
        }
        if (found.hasData()) {
            if (!last.hasData() || found.cpuTime > last.cpuTime) {
                last = found;
            }
            if (!first.hasData() || found.cpuTime < first.cpuTime || first.cpuTime == 0) {
                first = found;
            }
        }
    }

    if (first.cpuTime != 0 && last.cpuTime != 0 && last.cpuTime > first.cpuTime) {
        double elapsedCycles = (double)(last.cpuTime - first.cpuTime);
        last.cpuCyclesPerNano = elapsedCycles / (double)(last.clockTime - first.clockTime);
        *retCpuTime = last;
        return true;
    }

    return false;
}

static int64_t deriveGuestTimeDiff(
    const std::vector<char>& guestTrace,
    const std::vector<char>& hostTrace,
    int64_t tscOffset) {

    // First check for CPU time sync data in both traces.
    TraceCpuTimeSync hostSync, guestSync;
    bool hasHostSync = getTraceCpuTimeSync(hostTrace, &hostSync);
    if (!hasHostSync) {
        fprintf(stderr, "error: the host trace lacks timesync track events\n");
        return INT64_MAX;
    }
    bool hasGuestSync = getTraceCpuTimeSync(guestTrace, &guestSync);
    if (!hasGuestSync) {
        fprintf(stderr, "error: the guest trace lacks timesync track events\n");
        return INT64_MAX;
    }

    // Transform guest cpuTime to host:
    guestSync.cpuTime -= tscOffset;

    // Guest and host frequency measurement should match.
    double diffGuestHostFreq = abs(hostSync.cpuCyclesPerNano / guestSync.cpuCyclesPerNano - 1.0);
    if (diffGuestHostFreq > 0.0001) {
        fprintf(stderr, "warning: guest and host CPU frequencies off by %0.4f %%\n",
            100.0 * diffGuestHostFreq);
    }

    double cyclesPerNano = hostSync.cpuCyclesPerNano;
    double cyclesDelta = (double)getSignedDifference(hostSync.cpuTime, guestSync.cpuTime);
    int64_t offsetNs = (int64_t)(cyclesDelta / cyclesPerNano);
    double offsetSec = (double)offsetNs / 1000000000.0;
    if (offsetSec > 10.0) {
        fprintf(stderr, "warning: guest and host timesync track events off by %0.4fs\n", offsetSec);
    }
    return guestSync.clockTime + offsetNs - hostSync.clockTime;
}

VPERFETTO_EXPORT void combineTraces(const TraceCombineConfig* config) {
    std::vector<char> guestTrace;
    std::vector<char> hostTrace;

    std::ifstream guestFile(config->guestFile, std::ios::binary | std::ios::ate);
    std::ifstream::pos_type end = guestFile.tellg();
    guestFile.seekg(0, std::ios::beg);
    guestTrace.resize(end);
    guestFile.read(guestTrace.data(), end);
    guestFile.close();

    std::ifstream hostFile(config->hostFile, std::ios::binary | std::ios::ate);
    end = hostFile.tellg();
    hostFile.seekg(0, std::ios::beg);
    hostTrace.resize(end);
    hostFile.read(hostTrace.data(), end);
    hostFile.close();

    int64_t guestTimeDiff = deriveGuestTimeDiff(guestTrace, hostTrace, config->guestTscOffset);
    if (guestTimeDiff == INT64_MAX)
        return;

    std::vector<char> combinedTrace = constructCombinedTrace(hostTrace, guestTrace,
        -guestTimeDiff, config->addTraces);

    std::ofstream combinedFile(config->combinedFile, std::ios::out | std::ios::binary);
    combinedFile.write(combinedTrace.data(), combinedTrace.size());
    combinedFile.close();
}

} // namespace vperfetto

// Copyright 2022 Google LLC
// SPDX-License-Identifier: MIT

#include "tool-common.h"

#include "tool-info.h"
#include "tool-merge.h"
#include "tool-perfetto.h"
#include "tool-timesync.h"

struct tool_command {
    const char *name;
    const char *desc;
    int (*main)(int argc, char **argv);
};

static int
tool_help_main(int argc, char **argv);

static const struct tool_command tool_commands[] = {
    {
        .name = "help",
        .desc = "show this help message",
        .main = tool_help_main,
    },
    {
        .name = "info",
        .desc = "print information of a trace",
        .main = tool_info_main,
    },
    {
        .name = "merge",
        .desc = "merge host and guest traces",
        .main = tool_merge_main,
    },
    {
        .name = "perfetto",
        .desc = "start, stop, or wait perfetto tracing",
        .main = tool_perfetto_main,
    },
    {
        .name = "timesync",
        .desc = "generate track events with timesync debug annotations",
        .main = tool_timesync_main,
    },
};

static void
usage(int exit_code)
{
    printf("Usage: " TOOL_COMM " <command> [...]\n");
    printf("\n");
    printf("Valid commands are:\n");
    for (unsigned i = 0; i < ARRAY_SIZE(tool_commands); i++) {
        const struct tool_command *cmd = &tool_commands[i];
        const size_t len = strlen(cmd->name);

        char spaces[16];
        if (len >= sizeof(spaces)) {
            spaces[0] = ' ';
            spaces[1] = '\0';
        } else {
            const size_t c = sizeof(spaces) - len;
            memset(spaces, ' ', c);
            spaces[c] = '\0';
        }

        printf("  %s%s%s\n", tool_commands[i].name, spaces,
               tool_commands[i].desc);
    }

    exit(exit_code);
}

static int
tool_help_main(int argc, char **argv)
{
    usage(0);
    return 0;
}

int
main(int argc, char **argv)
{
    const char *tool_name = argc >= 2 ? argv[1] : "help";

    const struct tool_command *cmd = NULL;
    for (unsigned i = 0; i < ARRAY_SIZE(tool_commands); i++) {
        if (!strcmp(tool_commands[i].name, tool_name)) {
            cmd = &tool_commands[i];
            break;
        }
    }
    if (!cmd)
        usage(1);

    argc--;
    argv++;
    return cmd->main(argc, argv);
}

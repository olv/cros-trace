// Copyright 2022 Google LLC
// SPDX-License-Identifier: MIT

#include "tool-merge.h"

#include "vperfetto.h"

struct tool_merge {
    const char *output_trace;
    const char *dut_trace;
    const char *guest_trace;
    long long cpu_tick_offset;
};

static void
merge_usage(int exit_code)
{
    tool_info("Usage: " TOOL_COMM " merge"
              " --output-trace <MERGED_TRACE>"
              " --dut-trace <DUT_TRACE>"
              " --guest-trace <GUEST_TRACE>"
              " [--cpu-tick-offset <COUNT>]"
              " [--help]");
    exit(exit_code);
}

static void
merge_parse_options(struct tool_merge *merge, int argc, char **argv)
{
    for (int i = 0; i < argc; i++) {
        if (!strcmp(argv[i], "--output-trace") && i + 1 < argc)
            merge->output_trace = argv[i + 1];
        else if (!strcmp(argv[i], "--dut-trace") && i + 1 < argc)
            merge->dut_trace = argv[i + 1];
        else if (!strcmp(argv[i], "--guest-trace") && i + 1 < argc)
            merge->guest_trace = argv[i + 1];
        else if (!strcmp(argv[i], "--cpu-tick-offset") && i + 1 < argc)
            merge->cpu_tick_offset = atoll(argv[i + 1]);
        else if (!strcmp(argv[i], "--help") || !strcmp(argv[i], "-h"))
            merge_usage(0);
    }

    if (!merge->output_trace || !merge->dut_trace || !merge->guest_trace) {
        merge_usage(1);
    }
}

int
tool_merge_main(int argc, char **argv)
{
    struct tool_merge merge {};
    merge_parse_options(&merge, argc, argv);

    vperfetto::TraceCombineConfig config{};
    config.guestFile = merge.guest_trace;
    config.hostFile = merge.dut_trace;
    config.combinedFile = merge.output_trace;
    config.guestTscOffset = merge.cpu_tick_offset;

    vperfetto::combineTraces(&config);

    return 0;
}

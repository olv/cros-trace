// Copyright 2020 The Android Open Source Project
// SPDX-License-Identifier: Apache-2.0

#include <cinttypes>
#include <cstdint>

// Convenient declspec dllexport macro for android-emu-shared on Windows
#ifndef VPERFETTO_EXPORT
    #ifdef _MSC_VER
        #define VPERFETTO_EXPORT __declspec(dllexport)
    #else // _MSC_VER
        #define VPERFETTO_EXPORT __attribute__((visibility("default")))
    #endif // !_MSC_VER
#endif // !VPERFETTO_EXPORT

namespace vperfetto {

// An API to use offline to combine traces. The user can specify the guest/host trace files
// along with an optional argument for the guest clock boot time at start of tracing.
struct TraceCombineConfig {
    const char* guestFile;
    const char* hostFile;
    const char* combinedFile;

    // Use a tsc offset when deriving time sync between host and guest using rdtsc.
    int64_t guestTscOffset = 0;

    // Simply display the two separate traces in one trace. Do not modify them in any way.
    bool addTraces;
};

// Reads config.guestFile
// Reads config.hostFile
// Writes config.combinedFile
void combineTraces(const TraceCombineConfig* config);

} // namespace vperfetto

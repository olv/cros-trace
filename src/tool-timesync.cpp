// Copyright 2022 Google LLC
// SPDX-License-Identifier: Apache-2.0

#include "tool-timesync.h"

#include <ctime>
#include <perfetto.h>

#ifndef HAVE_RDTSC
#if defined(__x86_64__)
#define HAVE_RDTSC
#endif
#endif

#ifndef HAVE_CNTCVT
#if defined(__aarch64__)
#define HAVE_CNTCVT
#endif
#endif

PERFETTO_DEFINE_CATEGORIES(perfetto::Category("cros-trace")
                               .SetDescription("Chrome OS time sync events"));

PERFETTO_TRACK_EVENT_STATIC_STORAGE();

struct timesync {
    bool debug;
    bool background;
    int time;
};

static uint64_t
s2ns(uint64_t s)
{
    return s * 1000000000ull;
}

static uint64_t
ms2ns(uint64_t ms)
{
    return ms * 1000000ull;
}

static uint64_t
gettime(clockid_t id)
{
    struct timespec ts;
    if (clock_gettime(id, &ts))
        return 0;

    return s2ns(ts.tv_sec) + ts.tv_nsec;
}

static inline uint64_t
get_cpu_ticks()
{
#if defined(HAVE_RDTSC)
    uint32_t hi, lo;
    asm volatile("rdtsc" : "=a"(lo), "=d"(hi));
    return ((uint64_t)lo) | (((uint64_t)hi) << 32);
#elif defined(HAVE_CNTCVT)
    uint64_t vct;
    asm volatile("mrs %0, cntvct_el0" : "=r"(vct));
    return vct;
#else
#error "no cpu tick support"
#endif
}

static void
perfetto_annotate_time_sync(const perfetto::EventContext &perfetto)
{
    uint64_t boot_time = gettime(CLOCK_BOOTTIME);
    uint64_t cpu_time = get_cpu_ticks();
    uint64_t monotonic_time = gettime(CLOCK_MONOTONIC);
    // Read again to avoid cache miss overhead.
    boot_time = gettime(CLOCK_BOOTTIME);
    cpu_time = get_cpu_ticks();
    monotonic_time = gettime(CLOCK_MONOTONIC);

    auto *dbg = perfetto.event()->add_debug_annotations();
    dbg->set_name("clock_sync_boottime");
    dbg->set_uint_value(boot_time);
    dbg = perfetto.event()->add_debug_annotations();
    dbg->set_name("clock_sync_monotonic");
    dbg->set_uint_value(monotonic_time);
    dbg = perfetto.event()->add_debug_annotations();
    dbg->set_name("clock_sync_cputime");
    dbg->set_uint_value(cpu_time);
}

static void
timesync_trace(void)
{
    TRACE_EVENT_INSTANT(
        "cros-trace", "timesync",
        [&](perfetto::EventContext ctx) { perfetto_annotate_time_sync(ctx); });
}

static struct timespec
timesync_loop_interval(int ms)
{
    const struct timespec interval = {
        .tv_nsec = static_cast<long>(ms2ns(ms)),
    };
    return interval;
}

static void
timesync_loop_timeout(int seconds)
{
    const uint64_t end = gettime(CLOCK_MONOTONIC) + s2ns(seconds);

    const struct timespec sleep = timesync_loop_interval(100);
    do {
        timesync_trace();
        clock_nanosleep(CLOCK_MONOTONIC, 0, &sleep, NULL);
    } while (gettime(CLOCK_MONOTONIC) < end);
}

static void
timesync_loop(std::atomic_bool *run)
{
    std::atomic_bool always_true = true;
    if (!run)
        run = &always_true;

    const struct timespec sleep = timesync_loop_interval(100);
    do {
        timesync_trace();
        clock_nanosleep(CLOCK_MONOTONIC, 0, &sleep, NULL);
    } while (atomic_load_explicit(run, std::memory_order_relaxed));
}

static void
timesync_init_perfetto()
{
    perfetto::TracingInitArgs args;
    args.backends = perfetto::kSystemBackend;
    perfetto::Tracing::Initialize(args);
    perfetto::TrackEvent::Register();
}

static void *
timesync_thread(void *arg)
{
    auto *thrd = static_cast<struct timesync_thread *>(arg);

    timesync_init_perfetto();
    timesync_loop(&thrd->run);

    return NULL;
}

void
timesync_thread_join(struct timesync_thread *thrd)
{
    thrd->run = false;
    pthread_join(thrd->thread, NULL);
}

int
timesync_thread_create(struct timesync_thread *thrd)
{
    thrd->run = true;
    return pthread_create(&thrd->thread, NULL, timesync_thread, thrd);
}

static void
timesync_usage(int exit_code)
{
    tool_info("Usage: " TOOL_COMM " timesync"
              " [--debug]"
              " [--background]"
              " [--time <SECONDS>]"
              " [--help]");
    exit(exit_code);
}

static void
timesync_parse_options(struct timesync *timesync, int argc, char **argv)
{
    for (int i = 0; i < argc; i++) {
        if (!strcmp(argv[i], "--debug"))
            timesync->debug = true;
        else if (!strcmp(argv[i], "--background"))
            timesync->background = true;
        else if (!strcmp(argv[i], "--time") && i + 1 < argc)
            timesync->time = atoi(argv[i + 1]);
        else if (!strcmp(argv[i], "--help") || !strcmp(argv[i], "-h"))
            timesync_usage(0);
    }
}

int
tool_timesync_main(int argc, char **argv)
{
    struct timesync timesync {};

    timesync_parse_options(&timesync, argc, argv);

    if (timesync.debug)
        tool_dbg_enable();

    if (timesync.background) {
        if (tool_daemon(!timesync.debug, NULL))
            return 1;
    }

    timesync_init_perfetto();

    tool_info("generating timesync trace events...");
    if (timesync.time)
        timesync_loop_timeout(timesync.time);
    else
        timesync_loop(NULL);

    return 0;
}
